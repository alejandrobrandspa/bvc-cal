'use strict';
import React from 'react';
import {render} from 'react-dom';
import {Router, Route, browserHistory} from 'react-router';
import request from 'axios';
import jwtDecode from 'jwt-decode';
import App from './components/app';
import NoMatch from './components/no_match';
import Admin from './components/admin';
import Events from 'admin/event/section';
import Register from 'user/register';
import Login from 'user/login';
import Recover from 'user/recover';


function checkAuthAdmin(next, replace) {
  var token = localStorage.getItem('x-t');

  if(token) {
    var decoded = jwtDecode(token);
    if(decoded && decoded.admin) {
        return true;
    }
  }

  return replace('/login');
};

function checkAuth(next, replace) {
  var token = localStorage.getItem('x-t');

  if(token) {
    var decoded = jwtDecode(token);
    if(decoded) {
        return true;

    }
  }

  return replace('/login');
};

if(localStorage.getItem('x-t')) {
  request.defaults.headers.common['x-access-token'] = localStorage.getItem('x-t');
}

request.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    // Do something with response error
    if(error.status == 401) return window.location = '/login';

    return Promise.reject(error);
});

render((
  <Router history={browserHistory} >
    <Route path="/admin/login" component={Login} />
    <Route path="/login" component={Login} />
    <Route path="/confirmation/:id" component={Login} />
    <Route path="/recover/:id" component={Recover} />
    <Route path="/admin" component={Admin} onEnter={checkAuthAdmin}>
      <Route path="/admin/event/:id" component={Events} />
    </Route>
    <Route path="/" component={App} onEnter={checkAuth} />
    <Route path="/register" component={Register} />
    <Route path="*" component={NoMatch}/>
  </Router>
), document.getElementById('app-calendar'));

FROM ubuntu:14.04

MAINTAINER ale@developersoul.com

ENV DEBIAN_FRONTEND noninteractive

# update packages
RUN sudo apt-get update

# install tools
RUN apt-get install -y curl git wget dialog build-essential python

# add node source
RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash - && sudo apt-get install -y nodejs

RUN npm install -g pm2

# install nginx and nodejs
RUN sudo apt-get install -y nginx

#remove nginx config
RUN rm -v /etc/nginx/nginx.conf

# copy conf to the next folder
ADD nginx.conf /etc/nginx/

# Add keyserver for mongodb
RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10

# listfile mongodb
RUN echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list

# update packages and install mongodb
RUN sudo apt-get update && sudo apt-get install -y mongodb-org

# Create the MongoDB data directory
RUN mkdir -p /data/db
VOLUME ["/data/db"]

# create folder
RUN mkdir -p /var/www/my-app
ADD . /var/www/my-app
VOLUME ["/var/www/my-app"]

# Define working directory
WORKDIR /var/www/my-app

RUN npm install

# port
EXPOSE 80

# run services
CMD sudo service mongod start
CMD service nginx start
CMD npm run deploy

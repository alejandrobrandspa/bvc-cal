'use srict';
import React from 'react';
import Modal from 'react-modal';
import $ from 'jquery';
import Tabs from 'lib/tabs';

var customStyle = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(0, 0, 0, 0.75)',
    zIndex: '999'
  },

  content : {
    maxWidth: '650px',
    minHeight: '400px',
    margin: '0 auto',
    background                 : '#f1f1f1',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '0',
    outline                    : 'none',
    padding: 0,
  }
};

var imgStyle = {
  width: '100%'
};

var contentStyle = {
  padding: '0 15px 30px 15px'
};

var closeStyle = {
  position: 'absolute',
  right: '15px',
  top: '5px',
  fontSize: '32px',
  color: '#E1E1E1'
}

var headerStyle = {
  'background': '#fff',
  'padding': '15px',
  'marginBottom': '15px',
  'borderBottom': '1px solid #E2E2E2'
}

module.exports = React.createClass({

  getInitialState() {
    return {

    }
  },

  componentDidMount() {

  },

  selectValue(e) {
    $(e.currentTarget).select();
  },

  render() {

    return (
      <Modal
        isOpen={this.props.isOpenModal}
        style={customStyle} className="modal-sync">

        <a href="#" onClick={this.props.toggleModal} style={closeStyle}>
          <i className="icon ion-android-close"></i>
        </a>

        <div className="header" style={headerStyle}>
          <h4 style={{
            'margin': '0 auto',
            padding: '10px 15px',
            textAlign: 'center',
            color: 'red',
            fontSize: '14px',
            border: '1px solid red',
            textAlign: 'middle',
            width: '171px',
            textTransform: 'uppercase'
          }}> <i style={{fontSize: '22px'}} className="icon ion-android-sync"></i> Sincronizar</h4>
        </div>

        <div style={contentStyle} className="content">
          <p style={{textAlign:"center", color: "#414141", fontSize: '17px', fontWeight: 200}}>Puede enviar las fechas de los Calendarios BVC a su agenda<br/>personal, utilizando los siguientes medios:</p>
          <Tabs />
        </div>

      </Modal>
    )
  }
});

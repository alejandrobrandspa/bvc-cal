'use strict';
import React from 'react';
import Calendar from 'calendar/section';
import Header from './header';
import WelcomeModal from './welcome_modal';

module.exports = React.createClass({
  getInitialState() {
    return {
      isOpenModal: false
    }
  },

  componentDidMount() {
    if(!localStorage.getItem('modalClosed')) {
      this.setState({isOpenModal: true});
    }
  },

  toggleModal(e) {
    e.preventDefault();
    this.setState({isOpenModal: false});

    localStorage.setItem('modalClosed', true);
  },

  render() {
    return (
      <div>

        <WelcomeModal
          toggleModal={this.toggleModal}
          isOpenModal={this.state.isOpenModal}
        />
        <Header />
          <div className="container">
            {this.props.children || <Calendar />}
          </div>
          <hr/>
          <footer>
            <div className="container">
              <h5 className="pull-right">&copy; 2016 Calendario BVC </h5>
            </div>
          </footer>
      </div>
    );
  }
});

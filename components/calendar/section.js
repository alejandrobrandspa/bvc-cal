'use strict';
import React from 'react';
import $ from 'jquery';
import request from 'axios';
import fullCalendar from 'fullcalendar';
import Select from 'react-select';
import _ from 'lodash';
import 'fullcalendar/dist/lang/es';
import '../gcal';
import Drop from 'tether-drop';
import moment from 'moment';
import SyncModal from '../sync_modal';
import ConventionsModal from '../conventions_modal';
import calendarOptions from 'lib/calendar_options';

module.exports = React.createClass({
  getInitialState() {
    return {
      events: [],
      selectValue: null,
      showSyncModal: false,
      showConventionsModal: false,
    }
  },

  fetchEvents(find) {
    var find = JSON.stringify(find);

    request
    .get('/api/events', {
      params: {
        populate: 'calendar',
        find: find
      }
    })
    .then(res => this.attachEvents(res));
  },

  attachEvents(res) {
    let calendar = $('#calendar');
    let data = res.data;

    this.setState({events: data});

    //clean events
    calendar.fullCalendar( 'removeEvents' );

    let colors = {
      '56d5f5b3d52b07b496e4bed6': '#51B749',
      '56d5f5d822e6efe796ff01f2': '#3D67AC',
      '57433bc1afcc67854a8a6d72': '#DC2127',
      '57433bebafcc67854a8a6d74': '#5484ED',
      '57433bceafcc67854a8a6d73': '#B90E28',
      '57435d2ce0861a027247d796': '#FCB62D',
      '576859f262aa12ad04967c06': '#A91C3E'
    };

    let events = data.reduce((obj, event) => {

      if(event.calendar && event.calendar._id) {
        (obj[event.calendar._id] || (obj[event.calendar._id] = [])).push(event);
      }

      return obj;
    }, {});

    _.forEach(events, (cal, key) => {

      calendar.fullCalendar('addEventSource', _.extend({
        events: cal,
        color: colors[key],
        className: 'apievent'
      }));
    });
  },

  configFullCalendar() {
    $('#calendar').fullCalendar({
      header: {
          left:   'prev next today',
          center: 'title',
          right:  ' agendaDay ,agendaWeek, month'
      },

      eventClick: (event, jsEvent, view ) => {
        this.showEventInfo(event, jsEvent, view);
      }
    });
  },

  showEventInfo(event, jsEvent, view) {
    var drop;
    jsEvent.preventDefault();
    var template = _.template($('#drop-template').html());

    var item = _.filter(this.state.events, o => o.id == event.id);

    item = item[0];

    var start = moment(item.start).format('ddd D MMMM HH:mm');
    var end = moment(item.end).format('ddd D MMMM HH:mm');

    if(moment(item.start)._f == "YYYY-MM-DD") {
      var start = moment(item.start).format('ddd D MMMM');
      var end = moment(item.end).format('ddd D MMMM');
    }


    var html = template({
      title: item.title,
      start: start,
      end: end,
      calendar: item.calendar.title,
      location: item.location,
      description: item.description
    });

    let target = jsEvent.currentTarget;
    let center = $('body').width() / 2;
    let position = 'top center';

    if($(target).position().left < center) {
      position = 'top left';
    }

    if($(target).position().left > center) {
      position = 'top right';
    }


    if(drop && drop.destroy) {
      drop.destroy()
    };

    drop = new Drop({
      target: target,
      content: html,
      openOn: 'click',
      position: position
    });

    drop.on('close',function() {
      this.remove();
    });

    drop.open();

  },

  isntEmpty(val) {
    if(val !== '') return true;
  },

  setMonthCode() {
    let months = {
      'enero': '/f',
      'febrero': '/g',
      'marzo': '/h',
      'abril': '/j',
      'mayo': '/k',
      'junio': '/m',
      'julio': '/n',
      'agosto': '/q',
      'septiembre': '/u',
      'octubre': '/v',
      'noviembre': '/x',
      'diciembre': '/z'
    };

    let month = $('.fc-toolbar h2').text().split(' ');

    if( this.isntEmpty(month[1])) {

      month = `
      <span class="subtitle">FECHAS MERCADO DE DERIVADOS</span>
      <br/>${month[0]}${months[month[0]]}
      <span class="year">${month[1]}</span>
      `;

      $('.fc-toolbar h2').html(month);
    }
  },

  changeCalendar(cal) {
    this.setState({selectValue: cal});

    if(!_.isEmpty(cal)) {
      cal = cal.split(',');
      let calendar = { calendar: { $in: cal }};
      this.fetchEvents(calendar);
    } else {
      this.fetchEvents(null);
    }
  },

  handleDownload(e) {
    e.preventDefault();
    request('/api/events/download')
    .then((res) => {
      window.location= '/ics/bvc-calendario.ics';
    });
  },

  toggleSyncModal(e) {
    e.preventDefault();
    this.setState({showSyncModal: !this.state.showSyncModal});
  },

  toggleConventionsModal(e) {
    e.preventDefault();
    this.setState({showConventionsModal: !this.state.showConventionsModal});
  },

  componentDidMount() {
    this.configFullCalendar();
    this.fetchEvents();
    this.setMonthCode();

    $(`#calendar .fc-month-button,
      #calendar .fc-next-button,
      #calendar .fc-prev-button,
      #calendar .fc-today-button`)
    .on('click', e => this.setMonthCode());
  },

  render() {
    var options = calendarOptions;

    return (
      <div id="calendar-section" className="animated fadeIn">
      <ConventionsModal
        toggleModal={this.toggleConventionsModal}
        isOpenModal={this.state.showConventionsModal}
      />

        <SyncModal
          toggleModal={this.toggleSyncModal}
          isOpenModal={this.state.showSyncModal}
        />

        <div className="row">
          <div className="col-md-4">
            <Select
                multi={true}
                value={this.state.selectValue}
                placeholder={"Seleccionar calendario"}
                options={options}
                searchable={false}
                onChange={this.changeCalendar}
            />
          </div>

          <div className="col-md-4"></div>

          <div className="col-md-4">
            <a
              href="#"
              className="btn__download"
              onClick={this.toggleSyncModal}>
              <i className="icon ion-android-sync"></i> Sincronizar
            </a>
            <a
              href="#"
              className="btn__conventions"
              onClick={this.toggleConventionsModal}>
              <img style={{width: "22px"}} src="/img/compass.png" alt=""/> Convenciones
            </a>

          </div>
        </div>
        <br/>
        <div id="calendar"></div>
        <div className="col-md-2" style={{marginTop: '20px'}}>

        </div>

      </div>
    );
  }
});

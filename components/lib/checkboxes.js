'use srict';
import React from 'react';
import request from 'axios';

module.exports = React.createClass({

  getInitialState() {
    return {
      calendars: []
    }
  },

  componentDidMount() {
    request
    .get('/api/calendars')
    .then(response => { return response.data})
    .then(calendars => {
      console.log(calendars);
      this.setState({calendars: calendars})
    })
    .catch(function (response) {
      console.log(response);
    });
  },

  render() {
    var checkboxes = this.state.calendars.map(cal => {
      return (
        <div
          key={cal._id}
          className="checkbox"
          style={{textAlign: 'left'}}
          >
          <label>
            <input
              type="checkbox"
              value={cal._id}
              onClick={this.props.onChange}/> {cal.title}
          </label>
        </div>
      );
    });

    return (
      <section className="calendar-options-checks" style={{width: '300px'}}>
        {checkboxes}
      </section>
    )
  }
});

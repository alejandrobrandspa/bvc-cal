'use srict';
import React from 'react';
import $ from 'jquery';
import _ from 'lodash';
import request from 'axios';
import Checkboxes from './checkboxes';
import DownloadPanel from './download_panel';
import GoogleCalendarPanel from './google_calendar_panel';
import OutlookCalendarPanel from './outlook_calendar_panel';

module.exports = React.createClass({

  getInitialState() {
    var base = window.location.origin;
    var token = localStorage.getItem('x-t') || '';
    return {
      calendarsSelected: [],
      calendarsJoined: '',
      downloadURL: base + '/api/events/download?token=' + token,
      showError: false
    }
  },

  handleClick(e) {
    if(e) e.preventDefault();
    var $current = $(e.currentTarget);
    var i = $current.data('panel');
    $('.tabs-menu li').removeClass('active');

    $current.parent().addClass('active');
    [1,2,3].map(num => {
      return $('.tab-panel-' + num).addClass('hidden');
    });

    $('.tab-panel-' + i).removeClass('hidden');
  },

  handleOptions(e) {
    var value = $(e.currentTarget).val();
    var calendars = [];
    if($(e.currentTarget).prop( "checked" )) {
      calendars =  this.state.calendarsSelected.concat([value]);
    } else {
      calendars =  _.without(this.state.calendarsSelected, value);
    }

      this.setState({
        calendarsSelected: calendars
      });

    var calendarsJoined = calendars.map(cal => {
      return 'calendars[]=' + cal;
    }).join('&');

    this.setState({
      calendarsJoined: calendarsJoined
    })

  },

  handleDownload(e) {
    if(e) e.preventDefault();
    var calendars = this.state.calendarsSelected;

    var url = this.state.downloadURL + '&' + this.state.calendarsJoined;

    if(!_.isEmpty(calendars)) {
      this.setState({showError: false});
      request
      .get(url)
      .then(res => {
        return window.location = url;
      });
    } else {
      this.setState({showError: true});
    }
  },

  render() {
    var url = this.state.downloadURL;

    if(!_.isEmpty(this.state.calendarsJoined)) {
      url = url + '&' + this.state.calendarsJoined;
    }

    return (
      <div className='sync-tabs'>
        <nav className='tabs-navigation'>
          <ul className='tabs-menu'>
            <li className="active">
              <a data-panel="1"  href="#" onClick={this.handleClick}><img src="img/download.png"  width="24"/> Descargar</a>
            </li>
            <li>
              <a data-panel="2" href="#" onClick={this.handleClick}><img src="img/logo_calendar.png"  width="24"/> Google calendar</a>
            </li>
            <li>
              <a data-panel="3"  href="#" onClick={this.handleClick}><img src="img/logo_outlook.svg"  width="24"/> Outlook web</a>
            </li>
          </ul>
        </nav>

        <section className="panels">
        <div
          className={this.state.showError ? "alert alert-danger" : "hidden"}>
          Seleccione al menos un mercado antes de descargar
        </div>

          <DownloadPanel onChange={this.handleOptions} handleDownload={this.handleDownload}/>
          <GoogleCalendarPanel url={url}  onChange={this.handleOptions}/>
          <OutlookCalendarPanel url={url} onChange={this.handleOptions} />

        </section>
        </div>
    )
  }
});

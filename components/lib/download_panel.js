'use srict';
import React from 'react';
import Checkboxes from './checkboxes';

module.exports = React.createClass({

  render() {
    return (
      <article className='tab-panel tab-panel-1'>

        <p style={{fontSize: '15px', fontWeight: '300', color: '#4f4f4f'}}>
          Con esta opción sincronizar el calendario a su aplicación de escritorio.
        </p>
        <p style={{margin: '20px 0', fontWeight: '300', color: '#4f4f4f  '}}>
          <span style={{color: '#FF0000', fontWeight: 'normal'}}>1.</span> Seleccione los mercados que desea sincronizar
        </p>
        <Checkboxes onChange={this.props.onChange} />
        <p style={{margin: '20px 0', fontWeight: '300', color: '#4f4f4f  '}}>
          <span style={{color: '#FF0000', fontWeight: 'normal'}}>2.</span> Haga clic "Descargar"
        </p>
        <a
          href="#"
          onClick={this.props.handleDownload}
          className="download"
          style={{textTransform: 'uppercase',display: 'block',width: '150px', margin: '0 auto', float: 'none', padding: '10px', border: '1px solid red'}}
        >
          Descargar
        </a>
        <p style={{margin: '20px 0', fontWeight: '300', color: '#4f4f4f  '}}>
          <span style={{color: '#FF0000', fontWeight: 'normal'}}>3.</span> Abra el archivo "bvc-calendario" que encontrará en su carpeta de descargas
        </p>
        <p style={{margin: '20px 0', fontWeight: '300', color: '#4f4f4f  '}}>
          <span style={{color: '#FF0000', fontWeight: 'normal'}}>4.</span> Haga clic en el botón "Aceptar"
        </p>
      </article>
    )
  }
});

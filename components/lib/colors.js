'use strict';

module.exports = [
  {
    value: '56d5f5b3d52b07b496e4bed6',
    label: 'TASA DE CAMBIO',
    color: '#51B749'
  },
  {
    value: '56d5f5d822e6efe796ff01f2',
    label: 'INFLACIÓN',
    color: '#3D67AC'
  },
  {
    value: '57433bc1afcc67854a8a6d72',
    label: 'ACCIONES',
    color: '#DC2127'
  },
  {
    value: '57433bebafcc67854a8a6d74',
    label: 'TES',
    color: '#5484ED'
  },
  {
    value: '57433bceafcc67854a8a6d73',
    label: 'COLCAP',
    color: '#B90E28'
  },
  {
    value: '57435d2ce0861a027247d796',
    label: 'FESTIVOS COLOMBIA',
    color: '#FCB62D'
  },
  {
    value: '576859f262aa12ad04967c06',
    label: 'FESTIVOS USA',
    color: '#A91C3E'
  }
];

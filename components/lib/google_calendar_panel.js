'use srict';
import React from 'react';
import Checkboxes from './checkboxes';
module.exports = React.createClass({

  getInitialState() {
    return {

    }
  },

  render() {
    var base = window.location.origin;
    var token = localStorage.getItem('x-t') || '';
    var url = base + '/api/events/download?token=' + token;

    return (
      <article className='tab-panel tab-panel-2 hidden'>

        <ul style={{textAlign: 'center', float: 'left'}}>
          <li>Esta sincronización solo funciona vía web, si desea sincronizar su aplicación de escritorio utilice la opción DESCARGAR</li>
          {/*<li>
            <b style={{color: 'red', fontWeight: 'normal'}}>1</b> · Seleccione los mercados que desea sincronizar
          </li>*/}
          {/*<li>
            <Checkboxes onChange={this.props.onChange} />
          </li>*/}
          <li>
            <b style={{color: 'red', fontWeight: 'normal'}}>1</b> · Ingrese a “Otros Calendarios” en su Google Calendar y escoja la opción: Añadir por URL
          </li>
          <li>
            <img style={{margin: '0 auto'}} src="img/google_sync_2.png" className="img-responsive" />
          </li>
          <li>
            <b style={{color: 'red', fontWeight: 'normal'}}>2</b> · Ingrese la siguiente URL en el campo indicado:
          </li>
          <li>
            <input
               value={url}
               onClick={this.selectValue}
               className="form-control select-value"
             />
          </li>
          <li>
              <img style={{margin: '0 auto'}} src="img/google_sync_3.png" className="img-responsive"/>
          </li>
          <li><b style={{color: 'red', fontWeight: 'normal'}}>3</b> · Haga clic en "Añadir calendario"</li>
        </ul>
      </article>
    )
  }
});

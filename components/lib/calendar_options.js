'use strict';

module.exports = [
  { value: '56d5f5b3d52b07b496e4bed6', label: 'TASA DE CAMBIO' },
  { value: '56d5f5d822e6efe796ff01f2', label: 'INFLACIÓN' },
  { value: '57433bc1afcc67854a8a6d72', label: 'ACCIONES' },
  { value: '57433bebafcc67854a8a6d74', label: 'TES' },
  { value: '57433bceafcc67854a8a6d73', label: 'COLCAP' },
  { value: '57435d2ce0861a027247d796', label: 'FESTIVOS COLOMBIA' },
  { value: '576859f262aa12ad04967c06', label: 'FESTIVOS USA' }
];

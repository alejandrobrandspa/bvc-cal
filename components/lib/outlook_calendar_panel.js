'use srict';
import React from 'react';
import Checkboxes from './checkboxes';

module.exports = React.createClass({

  getInitialState() {
    return {

    }
  },

  componentDidMount() {

  },

  render() {
    var url = this.props.url;

    return (
      <article className='tab-panel tab-panel-3 hidden'>

        <ul style={{textAlign: 'center', float: 'left'}}>
          <li>Esta sincronización solo funciona vía web, si desea sincronizar su aplicación de escritorio utilice la opción DESCARGAR</li>
          <li>
            <b style={{color: 'red', fontWeight: 'normal'}}>1</b> · Seleccione los mercados que desea sincronizar
          </li>
          <li>
            <Checkboxes onChange={this.props.onChange} />
          </li>
          <li>
            <b style={{color: 'red', fontWeight: 'normal'}}>2</b> · Ingrese a “Agregar Calendario” en su Outlook Calendar y escoja la opción: Desde Internet
          </li>
          <li>
            <img style={{margin: '0 auto'}} src="img/outlook_sync_1.png" className="img-responsive" />
          </li>
          <li>
            <b style={{color: 'red', fontWeight: 'normal'}}>3</b> · Ingrese la siguiente URL en el campo indicado:
          </li>
          <li>
            <input
               value={url}
               onClick={this.selectValue}
               className="form-control select-value"
             />
          </li>
          <li>
              <img  style={{maxWidth: '500px',margin: '0 auto'}} src="img/outlook_sync_2.png" className="img-responsive"/>
          </li>
          <li>
            <b style={{color: 'red', fontWeight: 'normal'}}>4</b> · Asigne al calendario el nombre de su preferencia.
          </li>

          <li>
            <b style={{color: 'red', fontWeight: 'normal'}}>5</b> · Haga clic en guardar.
          </li>
        </ul>
      </article>
    )
  }
});

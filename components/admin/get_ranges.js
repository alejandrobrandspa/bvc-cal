'use strict';
import moment from 'moment';
require('moment-range');

module.exports = ({
  dateStart,
  dateEnd,
  cycle = 'startOf',
  dateFormat = 'DD-MM-YYYY',
  by = 'months'
}) => {

  let start;
  let end;
  console.log('get_ranges', dateStart, dateEnd);

  if(cycle === 'startOf') {
    start = moment(dateStart, dateFormat).startOf('month');
    end = moment(dateEnd, dateFormat);
  }

  if(cycle === 'endOf') {
    start = moment(dateStart, dateFormat).endOf('month');
    console.log(start);
    end = moment(dateEnd, dateFormat).endOf('month');
  }

  let range = moment.range(start, end);

  return range.toArray(by).map(date => moment(date).format(dateFormat));
};

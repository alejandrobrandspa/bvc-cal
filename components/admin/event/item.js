'use strict';
import React from 'react';
import {Link} from 'react-router';
import moment from 'moment';
import Form from './form';

module.exports = React.createClass({

  getInitialState() {
    return  {
      model: {},
      lastCalendar: {},
      edit: false
    }
  },

  componentDidMount() {
    this.setState({
      model: this.props.model,
      lastCalendar: this.props.model.calendar
    });
  },

  handleDestroy(item, e) {
    e.preventDefault();
    this.props.onDestroy(item);
  },

  handleEdit(item, e) {
    e.preventDefault();
    this.setState({edit: true});
  },

  handleCancel() {
    var model = _.extend(this.state.model, {calendar: this.state.lastCalendar});
    this.setState({
      edit: false,
      model: model
    });
  },

  handleUpdate(event) {
    this.setState({model: event, edit: false});
  },

  render() {
    var item = this.state.model;
    var calendar = item.calendar || {};
    var start = moment(item.start).format("DD-MM-YYYY HH:mm");
    var end = moment(item.end).format("DD-MM-YYYY HH:mm");
    var form;

    if(this.state.edit) {
      form = <Form
        model={item}
        onCancel={this.handleCancel}
        updated={this.handleUpdate}
        />;
    }

    if(moment(item.start)._f == 'YYYY-MM-DD') {
      start = moment(item.start).format("DD-MM-YYYY");
    }

    if(moment(item.end)._f == 'YYYY-MM-DD') {
      end = moment(item.end).format("DD-MM-YYYY");
    }

    return (
      <div>
        <ul className="list">
          <li>{calendar.title ? calendar.title : "No ingreso calendario"}</li>
          <li>{item.title}</li>
          <li>{start}</li>
          <li>{end}</li>
          <li className="options">
            <a href="#" onClick={this.handleEdit.bind(this, item)}> Editar</a>
            <br/>
            <a className="" href="#" onClick={this.handleDestroy.bind(this, item)}>Borrar</a>
          </li>
        </ul>
        <div className="edit-form-container">
          {form}
        </div>
      </div>
    );
  }
});

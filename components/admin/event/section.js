'use strict';
import React from 'react';
import request from 'axios';
import _ from 'lodash';
import Form from './form';
import List from './list';
import $ from 'jquery';
import moment from 'moment';

module.exports = React.createClass({
  getInitialState() {
    return {
      events: [],
      event: {},
      showForm: false
    }
  },

  fetch(find) {
    find = JSON.stringify(find);

    return request
    .get('/api/events', {
      params: {
        populate: 'calendar',
        find: find
      }
    })
  },

  componentDidMount() {
    $('body').css("background", "url('/img/bg_register.jpg')");
    this.fetch().then(res => this.setState({events: res.data || []}) );
  },

  add(event) {
    var events = [event].concat(this.state.events);
    this.setState({events: events, showForm: false});
  },

  update(event) {
    var events = this.state.events.map((item) => {
      if(item.id == event.id) {
        return  _.extend(item, event);
      }
      return item;
    });

    this.setState({events: events});
  },

  handleDestroy(event) {
    var events = this.state.events;

    request
    .delete('/api/events/' + event.id)
    .then((res) => {
      events = _.reject(events, item => {
        return item.id == event.id
      });

      this.setState({events: events});
    });
  },

  toggleCreate(e) {
    if(e) e.preventDefault();
    this.setState({showForm: !this.state.showForm});
  },

  filterBetweenDates(events, data) {
    let dateFormat = 'YYYY-MM-DD';

    if(data.dateStart && data.dateEnd) {
      let start = moment(data.dateStart, dateFormat);
      let end = moment(data.dateStart, dateFormat);
      return events.filter(event => moment(event.start, dateFormat).isBetween(data.dateStart, data.dateEnd));
    }

    return events;
  },

  filterEvents(data) {
    let evts = this.state.events;
    let find = {};

    if(data.calendars.length > 0) {
      find = { calendar: { $in: data.calendars }};
    }

    this.fetch(find)
    .then(res => this.filterBetweenDates(res.data, data))
    .then(events => this.setState({events: events}));
  },

  
  render() {
    return (
      <div className="animated fadeIn">

        <div className="col-sm-12" id="events-admin">
          <div className="panel panel-default">
            <div className="panel-heading">
            <h4>Crear evento</h4>
            <a href="#" onClick={this.toggleCreate}>+</a>
            </div>

            <div className="panel-body">
              <div className={this.state.showForm ? "form-create animated fadeIn" : "hidden"}>
              <Form
                stored={this.add}
                updated={this.update}
                params={this.props.params}
                onCancel={this.toggleCreate}
               />
               </div>

               <div className="list-events">
                  <h4 className="title-list">Eventos</h4>
                  <List
                    events={this.state.events}
                    onDestroy={this.handleDestroy}
                    onFilterEvents={this.filterEvents}
                   />

                 </div>
            </div>
        </div>
        </div>

      </div>
    );
  }
});

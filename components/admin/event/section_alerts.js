'use srict';
import React from 'react';
import Alert from './alert';
import uid from 'uid';
import _ from 'lodash';

module.exports = React.createClass({

  getInitialState() {
    return {
      alerts: []
    }
  },

  componentWillReceiveProps(props) {
    this.setState({alerts: props.alerts});
  },

  handleAppend(e) {
    e.preventDefault();
    var data = {
      _id: uid(),
      str: 'minutes',
      num: '30'
    };

    var alerts = this.state.alerts.concat([data]);
    this.props.onChange(alerts);
    this.setState({alerts: alerts});
  },

  handleChange(alert) {
    var alerts = this.state.alerts.map((item) => {
      if(item._id == alert._id) {
        return  _.extend(item, alert);
      }
      return item;
    });
    
    this.props.onChange(alerts);
    this.setState({alerts: alerts});
  },

  handleRemove(alert) {
    var alerts = _.filter(this.state.alerts, (item) => {
      return item._id != alert._id;
    });
    this.setState({alerts: alerts});
    this.props.onChange(alerts);
  },

  render() {
    var alerts = this.state.alerts.map((alert) => {
      return (
        <Alert
        key={alert._id}
        alert={alert}
        onChange={this.handleChange}
        onRemove={this.handleRemove}
       />
      )
    });

    return (
      <div>
        <button className="btn btn-sm" onClick={this.handleAppend}>Agregar notificación</button>
        {alerts}
      </div>
    )
  }
});

'use strict';
import React from 'react';
import Event from './item';
import FilterList from './filter_list';

module.exports = React.createClass({
  onDestroy(event) {
    this.props.onDestroy(event);
  },

  render() {
    let events = this.props.events.map(item => {
      return <Event key={item.id} model={item} onDestroy={this.onDestroy} />
    });

    return (
      <div>
        <FilterList events={this.props.events} onChange={this.props.onFilterEvents} />
        <p></p>
        <div className="row">

        <ul className="list-titles">
          <li>Calendario</li>
          <li>título</li>
          <li>Empieza</li>
          <li>Termina</li>
          <li>Opciones</li>
        </ul>

      {events}
      </div>
    </div>

    );
  }
});

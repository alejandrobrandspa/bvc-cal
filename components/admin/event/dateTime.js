'use strict';
import React from 'react';
import $ from 'jquery';
import _ from 'lodash';
import uid from 'uid';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import Time from './time';

module.exports = React.createClass({

  handleDate(date) {
    var date = moment(date).format('YYYY-MM-DD');
    var time = moment(this.props.date).format('HH:mm');
    var newDate = moment(date + ' ' + time);
    if(this.props.allDay) {
      newDate = date;
    }

    this.props.onChange(newDate);
  },

  handleTime(time) {
    var date = moment(this.props.date).format('YYYY-MM-DD');
    var time = time.format('HH:mm');
    var newDate = moment(date + ' ' + time);
    var newDate = moment(date + ' ' + time);

    this.props.onChange(newDate);
  },

  render() {
    var date = moment(this.props.date, 'YYYY-MM-DD HH:mm');
    return (
      <div className="row">
        <div className={this.props.allDay ? "form-group col-md-12" : "form-group col-md-6"}>
          <DatePicker
            selected={date}
            onChange={this.handleDate}
          />
        </div>

          <div className={this.props.allDay ? "hidden" : "form-group col-md-6"}>
             <Time
              time={date}
              onChange={this.handleTime}
            />
          </div>
      </div>
    );
  }
});

'use strict';
var React = require('react');
import Select from 'react-select';

module.exports = React.createClass({

  getInitialState() {
    return {
      num: 2,
      str: 'days'
    }
  },

  componentDidMount() {
    if(!_.isEmpty(this.props.alert)) {
      this.setState(this.props.alert);
    }
  },

  handleStr(val) {
    var data = _.extend(this.state, {str: val});
    this.props.onChange(data);
    this.setState(data);
  },

  handleNum() {
    var data = _.extend(this.state, {num: this.refs.num.value});
    this.props.onChange(data);
    this.setState(data);
  },

  handleRemove(e) {
    e.preventDefault();
    this.props.onRemove(this.state);
  },

  render() {
    var options = [
      {value: 'minutes', label: 'minutos'},
      {value: 'hours', label: 'horas'},
      {value: 'days', label: 'Días'},
      {value: 'weeks', label: 'Semanas'}
    ];

    return (
      <div  className="row">
      <br/>
        <div className="form-group col-md-2">
          <input type="text" ref="num" value={this.state.num} onChange={this.handleNum} />
        </div>

        <div className="form-group col-md-4">
          <Select options={options} value={this.state.str} onChange={this.handleStr}/>
        </div>

        <div className="form-group col-md-6">
            <button className={"btn btn-sm"} onClick={this.handleRemove}>Borrar</button>
        </div>

      </div>
    );
  }
});

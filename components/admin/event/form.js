'use strict';
import React from 'react';
import moment from 'moment';
import request from 'axios';
import $ from 'jquery';
import _ from 'lodash';
import Select from 'react-select';
import DateTime from './dateTime';
import Alerts from './section_alerts';
import uid from 'uid';
import calendarOptions from 'lib/calendar_options';

module.exports = React.createClass({
  getInitialState() {
    var now = moment()

    now = this.flatTime(now).format('YYYY-MM-DD HH:mm');

    return {
      id: null,
      calendar: null,
      title: null,
      description: null,
      location: null,
      start: now,
      end: now,
      alerts: [],
      errors: null,
      allDay: false
    }
  },

  componentDidMount() {
    this.setEvent(this.props);
  },

  componentWillReceiveProps(props) {
    this.setEvent(props);
  },

  setEvent(props) {

    if (props.model) {
      var model = this.props.model;
      if(moment(this.props.model.start)._f == 'YYYY-MM-DD') {
        model = _.extend(this.props.model, {allDay: true});
      }

      if(_.isObject(model.calendar)) {
        model = _.extend(this.props.model, {calendar: model.calendar._id});
      }

      this.setState(model);
    }
  },

  flatTime(time) {
    if(time.format('mm') >= 30) {
      time = time.minutes(0).add(30, 'm');
    } else {
      time = time.minutes(0);
    }
    return time;
  },

  changeCalendar(id) {
    this.setState({calendar: id});
  },

  handleStart(date) {
    var start = moment(date).format('YYYY-MM-DD HH:mm');

    if(moment(date)._f == "YYYY-MM-DD") {
      start = moment(date).format('YYYY-MM-DD');
    }

    var end = this.state.end;
    var dates = {};

    if(moment(end).isBefore(start)) {
      dates = {
        start: start,
        end: start
      };

      this.setState(dates);
    } else {
      this.setState({
        start: start
      });
    }

  },

  handleEnd(date) {
    var start = this.state.start;
    var end = moment(date).format('YYYY-MM-DD HH:mm');

    if(moment(date)._f == "YYYY-MM-DD") {
      end = moment(date).format('YYYY-MM-DD');
    }

    if(moment(end).isBefore(start)) {
      return this.setState({end: this.state.end});
    } else {
      this.setState({end: end});
    }
  },

  handleFields() {
    var fields = {
      title: this.refs.title.value,
      description: this.refs.description.value,
      location: this.refs.location.value,
    };

    this.setState(_.extend(this.state, fields));
  },

  cancel(e) {
    e.preventDefault();
    if (this.state.id) {
      return this.props.onCancel();
    } else {
      this.clean();
      return this.props.onCancel();
    }
  },

  clean(e) {
    if(e) e.preventDefault();

    var data = {
      id: null,
      calendar: '',
      title: '',
      description: '',
      location: '',
      start: moment(),
      end: moment(),
      errors: null,
      alerts: []
    };

    this.setState(_.extend(this.state, data));
  },

  handleSubmit(e) {
    e.preventDefault();

    if(this.state.id) {
      this.update();
    } else {
      this.store();
    }
  },

  showErrors(errors) {
    var arr = [];

    _.mapValues(errors, (val) => {
      if(val.message == 'Cast to ObjectID failed for value "" at path "calendar"')
      {
        arr.push('Calendario requerido');
      } else {
        arr.push(val.message)
      }

    });

    this.setState({errors: arr});
  },

  cleanAlerts(data) {
    var alertsWithoutId = data.alerts.map((item) => {
      return _.omit(item, ['_id']);
    });

    return _.extend(data, {alerts: alertsWithoutId});
  },

  store() {
    var data = this.state;
    data = this.cleanAlerts(data);
    data = _.omit(data, ['id', 'errors']);

    request
    .post('/api/events?populate=calendar', data)
    .then((res) => {
      this.props.stored(res.data);
      this.clean();
    })
    .catch((err) => {
      this.showErrors(err.data.errors);
    });
  },

  update() {
    var data = this.state;
    var errors = [];
    data = this.cleanAlerts(data);
    data = _.omit(data, ['errors']);

    request
    .put('/api/events/' + data.id + '?populate=calendar', data)
    .then((res) => {
      this.props.updated(res.data);
      this.clean();
    })
    .catch((err) => {
      if(err && err.data) {
        this.showErrors(err.data.errors);
      }

    });
  },

  handleAlerts(alerts) {

    this.setState({alerts: alerts});
  },

  toggleAllDay() {
    var allDay;
    var start;
    var end;
    var now = moment();

    if(this.state.allDay) {
      allDay = false;
      start = this.flatTime(now).format('YYYY-MM-DD HH:mm');
      end = this.flatTime(now).format('YYYY-MM-DD HH:mm');
    } else {
      allDay = true;
      start = now.format('YYYY-MM-DD');
      end = now.format('YYYY-MM-DD');
    }

    this.setState({
      allDay: allDay,
      start: start,
      end: end
    });
  },

  render() {
    var options = calendarOptions;

    var btnText = 'Publicar';
    var errors = '';

    if(this.state.id) {
      btnText = 'Actualizar';
    }

    if(this.state.errors) {
      errors = this.state.errors.map((err) => {
        return <div>{err} <br/></div>
      });
    }

    return (
      <div>
      <form className="animated fadeIn" id="event-form" onSubmit={this.handleSubmit}>
        <div className={this.state.errors ? "alert alert-danger" : "hidden"}>
          {errors}
        </div>

        <div className="form-group">
          <Select
              value={this.state.calendar}
              placeholder={"Seleccionar calendario"}
              options={options}
              searchable={false}
              onChange={this.changeCalendar}
          />
        </div>

        <div className="checkbox">
          <label htmlFor="">
            <input type="checkbox" onChange={this.toggleAllDay} checked={this.state.allDay}/> Todo el día
          </label>
        </div>

        <div className="row">
          <div className="col-sm-6">
            <DateTime
              date={this.state.start}
              onChange={this.handleStart}
              allDay={this.state.allDay}
            />
          </div>

          <div className="col-sm-6">
            <DateTime
              date={this.state.end}
              onChange={this.handleEnd}
              allDay={this.state.allDay}
            />
            </div>
        </div>

          <div className="form-group">
            <input
              name="title"
              ref="title"
              type="text"
              placeholder="Título"
              className="form-control"
              onChange={this.handleFields}
              value={this.state.title}
            />
          </div>

          <div className="form-group">
            <textarea
              name="description"
              ref="description"
              className="form-control"
              placeholder="descripción"
              rows="3"
              onChange={this.handleFields}
              value={this.state.description}
              />
          </div>

          <div className="form-group">
            <input
              name="location"
              type="text"
              ref="location"
              placeholder="Lugar"
              className="form-control"
              onChange={this.handleFields}
              value={this.state.location}
              />
          </div>

          <div className="form-group">
            <Alerts
              onChange={this.handleAlerts}
              alerts={this.state.alerts}
            />
          </div>

          <div className="row">

            <div className="col-sm-6">
              <button className="btn btn-primary publish" style={{width: '100%'}}>{btnText}</button>
            </div>

            <div className="col-sm-6">
              <button className="btn btn-default" style={{width: '100%'}} onClick={this.cancel}>Cancelar</button>
            </div>
          </div>
        </form>
        </div>
    );
  }
});

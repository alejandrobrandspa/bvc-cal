'use strict';
const React = require('react');
const Datetime = require('react-datetime');
const moment = require('moment');
import Select from 'react-select';
import calendarOptions from 'lib/calendar_options';

module.exports = React.createClass({

  getInitialState() {
    let now = moment().format('YYYY-MM-DD');
    return {
      dateStart: now,
      dateEnd: now,
      calendars: []
    }
  },

  componentWillReceiveProps(newProps) {
    console.log('new', newProps);
    this.getDates(newProps);
  },

  getDates(newProps) {
    let evts = newProps.events;
    console.log(evts);

    if(evts.length > 0) {
      let start = evts[0].start;
      let end = _.last(evts).start;
      this.setState({dateStart: end, dateEnd: start});
    }

  },

  handleFilter() {
    this.props.onChange(this.state);
  },

  setDateStart(date) {
    this.setState({
      dateStart: date.format('YYYY-MM-DD')
    });
  },

  setDateEnd(date) {
    this.setState({
      dateEnd: date.format('YYYY-MM-DD')
    });
  },

  setCalendars(val) {
    if(val.length > 0) {
      let arr = val.split(',');
      this.setState({calendars: arr});
    } else {
      this.setState({calendars: []});
    }
  },

  render() {
    return (
      <div className="row">
        <div className="col-md-3">
          <Select
              multi={true}
              value={this.state.calendars}
              placeholder={"Seleccionar calendario"}
              options={calendarOptions}
              searchable={false}
              onChange={this.setCalendars}
          />
        </div>

        <div className="col-md-3">
          <Datetime
            value={this.state.dateStart}
            viewMode="years"
            timeFormat={false}
            closeOnSelect={true}
            onChange={this.setDateStart}  />
        </div>

        <div className="col-md-3">
          <Datetime
            value={this.state.dateEnd}
            viewMode="years"
            timeFormat={false}
            closeOnSelect={true}
            onChange={this.setDateEnd}  />
        </div>

        <div className="col-md-3">
          <button className="btn" onClick={this.handleFilter}>Filtrar</button>
        </div>

      </div>


    )
  }
});

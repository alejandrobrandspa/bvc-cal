'use strict';
import React from 'react';
import uid from 'uid';
import $ from 'jquery';
import moment from 'moment';
import timepicker from 'timepicker';

module.exports = React.createClass({
  getInitialState() {
    var id = uid();
    return {
      id: id,
      time: moment()
    }
  },

  componentWillReceiveProps(props) {
    this.setTime(props.time);
  },

  componentDidMount() {
    this.set();
    this.handleChange();
  },

  setTime(time) {
    $('.' + this.state.id).timepicker('setTime', this.parse(time));
  },

  parse(time) {
    var time = time || this.state.time;

    if(time.format('mm') >= 30) {
      time = time.minutes(0).add(30, 'm');
    } else {
      time = time.minutes(0);
    }

    return time.format('H:mm');
  },

  handleChange() {
    var input = $('.' + this.state.id);
    var time = moment();
    var val;

    input.on('change', (e) => {
      val = $(e.currentTarget).val();
      time = moment(val, 'HH:mm');
      this.props.onChange(time);
    });
  },

  set() {
    var input = $('.' + this.state.id);
    input.timepicker({
      'timeFormat': 'H:i',
      'scrollDefault': 'now'
    });
  },

  render() {
    return (
      <input
        type="text"
        className={this.state.id}
        />
    );
  }
});

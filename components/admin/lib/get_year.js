'use strict';
import _ from 'underscore';

module.exports = ({ collection = [], type = 'min'}) => {
  let val;

  val = collection.length ? _[type](collection, model => new Date(model.start)) : 0;

  return val;
};

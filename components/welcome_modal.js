'use srict';
import React from 'react';
import Modal from 'react-modal';

var customStyle = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(0, 0, 0, 0.75)',
    zIndex: '999'
  },

  content : {
    maxWidth: '650px',
    minHeight: '400px',
    margin: '0 auto',
    background                 : '#f1f1f1',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '0',
    outline                    : 'none',
    padding: 0,
    textAlign: 'center'
  }
};

var imgStyle = {
  width: '100%'
};

var contentStyle = {
  padding: '15px 30px'
};

var closeStyle = {
  position: 'absolute',
  right: '15px',
  top: '5px',
  fontSize: '32px',
  color: '#fff'
}


module.exports = React.createClass({

  getInitialState() {
    return {

    }
  },

  componentDidMount() {

  },

  render() {
    return (
      <Modal
        isOpen={this.props.isOpenModal}
        style={customStyle}
        >

        <a href="#" onClick={this.props.toggleModal} style={closeStyle}><i className="icon ion-android-close"></i></a>

        <img src="/img/email-header.jpg" style={imgStyle}  alt=""/>

        <div style={contentStyle} className="content">
        <h3>Bienvenido al Calendario<br /> de Derivados BVC</h3>
        <p>Perderse una fecha clave es cosa del pasado, en el Calendario de Derivados BVC, encontrará las fechas claves de los mercados OIS, TRM, ACCIONES, COLCAP, TES y FECHAS MACROECONÓMICAS como datos de inflación y eventos claves. Puede consultar el calendario o sincronizarlo con su aplicación de escritorio o Google Calendar.</p>

        </div>
      </Modal>
    )
  }
});

'use strict';
import React from 'react';
import Events from 'admin/event/section';
import Sidebar from 'react-sidebar';

module.exports = React.createClass({
  closeSession(e) {
    e.preventDefault();
    localStorage.removeItem('x-t');
    window.location= "/";
  },

  render: function() {
    return (
        <div>
        <nav className="navbar navbar-default">
          <div className="container-fluid">
            <div className="navbar-header">
              <a href="#" className="navbar-brand">Calendario BVC</a>
            </div>

            <ul className="nav navbar-nav">
              <li><a href="#" onClick={this.closeSession}>Cerrar sesion</a></li>
            </ul>
          </div>
        </nav>

        <div className="col-md-8" style={{'margin': '0 auto', 'float': 'none'}} >
          {this.props.children || <Events />}
        </div>

        <footer className="col-md-12">
          <div className="container-fluid">
            <h5 style={{'textAlign': 'center', 'color': '#fff'}}>&copy; 2016 Calendario BVC </h5>
          </div>
        </footer>
        </div>
    );
  }
});

'use strict';
import React from 'react';
import request from 'axios';
import _ from 'lodash';

module.exports = React.createClass({
  getInitialState() {
    return {
      name: '',
      lastname: '',
      email: '',
      password: '',
      success: false,
      errors: null
    }
  },

  handleChange() {
    var ref = this.refs;
    var data = {
      identification: ref.identification.value,
      name: ref.name.value,
      lastname: ref.lastname.value,
      mobile: ref.mobile.value,
      phone: ref.phone.value,
      company: ref.company.value,
      position: ref.position.value,
      email: ref.email.value,
      password: ref.password.value,
    };
    this.setState(data);
  },

  sendConfirmation(user) {
    request
    .get('/api/users/sendconfirmation/' + user._id)
    .then((res) => {
      this.props.onChange();
    })
    .catch((res) => {
      if(err.errors) {

      }
    });
  },

  showErrors(errors) {

    var arr = [];

    _.mapValues(errors, (val) => {
        arr.push(val.message)
    });

    this.setState({errors: arr});
  },

  handleSubmit(e) {
    e.preventDefault();
    var data = this.state;

    request
    .post('/api/users', data)
    .then((res) => {
      this.sendConfirmation(res.data);
    })
    .catch((res) => {
      this.showErrors(res.data.errors);
    });
  },

  render() {

    return (
      <div>
      <div className={this.state.errors ? "alert alert-danger" : "hidden"}>
        {this.state.errors ? this.state.errors.join(', ') : ""}
      </div>
        <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input
            ref="identification"
            type="text"
            className="form-control"
             placeholder="Número de cédula"
             onChange={this.handleChange}
             value={this.state.identification}
             />
        </div>

          <div className="form-group">
            <input
              ref="name"
              type="text"
              className="form-control"
               placeholder="Nombre"
               onChange={this.handleChange}
               value={this.state.name}
               />
          </div>

          <div className="form-group">
            <input
              ref="lastname"
              type="text"
              className="form-control"
               placeholder="Apellido"
               onChange={this.handleChange}
               value={this.state.lastname}
               />
          </div>

          <div className="form-group">
            <input
              ref="phone"
              type="text"
              className="form-control"
               placeholder="Teléfono fijo"
               onChange={this.handleChange}
               value={this.state.phone}
               />
          </div>

          <div className="form-group">
            <input
              ref="mobile"
              type="text"
              className="form-control"
               placeholder="Celular"
               onChange={this.handleChange}
               value={this.state.mobile}
               />
          </div>

          <div className="form-group">
            <input
              ref="company"
              type="text"
              className="form-control"
               placeholder="Empresa"
               onChange={this.handleChange}
               value={this.state.company}
               />
          </div>

          <div className="form-group">
            <input
              ref="position"
              type="text"
              className="form-control"
               placeholder="Cargo"
               onChange={this.handleChange}
               value={this.state.position}
               />
          </div>

          <div className="form-group">
            <input
              ref="email"
              type="text"
              className="form-control"
               placeholder="Email"
               onChange={this.handleChange}
               value={this.state.email}
               />
          </div>

          <div className="form-group">
            <input
              ref="password"
              type="password"
              className="form-control"
               placeholder="Password"
               onChange={this.handleChange}
               value={this.state.password}
               />
          </div>
          <button className="btn btn-default" >Registrarse</button>
        </form>
      </div>
    );
  }
});

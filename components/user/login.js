'use strict';
var React = require('react');
import request from 'axios';
import $ from 'jquery';

module.exports = React.createClass({

  getInitialState() {
    return {
      email: '',
      password: '',
      showConfimation: false,
      errors: null,
      messages: [],
      disableBtnRecover: false
    }
  },

  handleChange() {
    var data = {
      email: this.refs.email.value,
      password: this.refs.password.value,
    };
    this.setState(data);
  },

  handleSubmit(e) {
    e.preventDefault();
    var data = {
      email: this.state.email,
      password: this.state.password
    };
    request
    .post('/api/auth', data)
    .then((res) =>{

      if(res.data && res.data.token) {
        localStorage.setItem('x-t', res.data.token);
        window.location = '/';
      }
    })
    .catch((res) => {
      this.setState({errors: [res.data.message]});
    })
  },

  componentDidMount() {

    var id = this.props.params.id;
    var token = this.props.location.query.token;
    var passwordChanged = this.props.location.query.passwordChanged;

    if(passwordChanged) {
      this.setState({messages: ['Contraseña cambiada']})
    }

    if(id && token) {
      request
      .get('/api/users/confirmation/' + id + '?token=' + token)
      .then((res) => {
        this.setState({
          showConfimation: true,
          email: res.data.email
        });
      })
      .catch((err) => {
        console.log(err);
      });

    }

    $('body').css("background", "url('/img/bg_register.jpg')");
  },

 goToRegister() {
   window.location = "/register";
  },

  validateMail(mail) {
    let res = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g;
    return res.test(mail);
  },

  sendRecoverMail(e) {
    e.preventDefault();

    if(this.validateMail(this.state.email)) {
      request
      .post(`/api/users/sendrecover?email=${encodeURIComponent(this.state.email)}`)
      .then(res => {
        this.setState({
          messages: ['Revise su casilla de correo'],
          disableBtnRecover: true
        })
      });

      return false;
    }

    return this.setState({errors: ['ingrese el email para recuperar contraseña']});
  },

  render() {
    return (
      <div className="col-md-4" id="login">
        <div className="panel panel-default">
          <div className="panel-body">
          <div className="logo">
            <img src="/img/logo.svg" alt="BVC logo" width="300"/>
          </div>
            <div className={this.state.showConfimation ? "alert alert-warning" : "hidden"}>
              A sido confirmado, ahora ingrese su contraseña
            </div>
            <div className={this.state.messages.length > 0 ? "alert alert-warning" : "hidden"}>
              {this.state.messages ? this.state.messages.join(', ') : ""}
            </div>

            <div className={this.state.errors ? "alert alert-danger" : "hidden"}>
              {this.state.errors ? this.state.errors.join(', ') : ""}
            </div>
            <h5>Ingrese a su Calendario de Derivados BVC</h5>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input  ref="email"type="text" placeholder="Email" className="form-control" onChange={this.handleChange} value={this.state.email}/>
              </div>
              <div className="form-group">
                <input ref="password" type="password" placeholder="Password" className="form-control" onChange={this.handleChange} value={this.state.password}/>
              </div>

              <a href="#" onClick={this.sendRecoverMail} className={this.state.disableBtnRecover ? "hidden" : "pull-right"} style={{marginBottom: '15px', color: '#fff'}} >Recuperar contraseña</a>

              <button className="btn btn-primary login-btn">Ingresar</button>

            </form>
            <h5>¿Aún no tiene cuenta?</h5>
            <button className="btn btn-primary" onClick={this.goToRegister}>Registrarse</button>
          </div>
        </div>
      </div>
    );
  }
});

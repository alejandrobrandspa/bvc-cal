'use srict';
import React from 'react';
import $ from 'jquery';
import request from 'axios';
module.exports = React.createClass({

  getInitialState() {
    return {
        password: null,
        passwordConfirmation: null,
        showError: false
    }
  },

  componentDidMount() {
    $('body').css("background", "url('/img/bg_register.jpg')");
  },

  handleForm(e) {
    e.preventDefault();
    var data = {
      password: this.refs.password.value,
      passwordConfirmation: this.refs.passwordConfirmation.value,
    };

    this.setState(data);

    if(data.password == data.passwordConfirmation && data.password != '' && data.password != ' ') {
      this.store(data);
    } else {
      this.setState({showError: true});
    }
  },

  store(data) {
    var id = this.props.params.id;
    var token = this.props.location.query.token;
    var password = {password: data.password};
    var url = '/api/users/recover/' + id + '?token=' + token;

    request
    .post(url, password)
    .then((res) => {
      window.location = '/login?passwordChanged=true';
    })
    .catch((res) => {
      console.log(res.data);
    });
  },

  render() {
    return (
      <div>
        <div className="panel col-md-4" id="recover">
          <div className="panel-body">
            <div className="logo">
              Calendario <img src="/img/logo.png" alt="BVC logo"/>
            </div>
            <div className={this.state.showError ? "alert alert-danger" : "hidden" }>
              Las contraseñas no coinciden.
            </div>

            <form onSubmit={this.handleForm}>
              <div className="form-group">
                <input
                  style={{background: '#fff', border: 'none', borderBottom: '1px solid #fff'}}
                  ref="password"
                  type="password"
                  placeholder="Contraseña"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <input
                  style={{background: '#fff', border: 'none', borderBottom: '1px solid #fff'}}
                  ref="passwordConfirmation"
                  type="password"
                  placeholder="Confirmar contraseña"
                  className="form-control"
                />
              </div>
              <button className="btn">Guardar</button>
            </form>
          </div>
        </div>

      </div>
    )
  }
});

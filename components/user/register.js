'use strict';
import React from 'react';
import Form from './form';
import $ from 'jquery';
module.exports = React.createClass({
  getInitialState() {
    return {
      showMessage: false
    }
  },

  componentDidMount() {
    $('body').css("background", "url('/img/bg_register.jpg')");
  },

  handleChange() {
    this.setState({showMessage: true});
  },

  render() {
    return (
      <div className="col-sm-4 animated fadeIn" id="register">
      <div className="panel panel-default">
        <div className="panel-body">
        <div className="logo">
          <img src="/img/logo.svg" alt="BVC logo" width="300"/>
        </div>

        <div className={this.state.showMessage ? "hidden" : ""}>
          <h5>Ingrese sus datos y cree su cuenta:</h5>
          <Form id="register-form" onChange={this.handleChange} />
        </div>
        <div className={this.state.showMessage ? "animated fadeIn": "hidden"}>
            <h5>Hemos enviado la confirmación de este registro a su correo electrónico.</h5>
        </div>
        <h5>¿Ya tiene cuenta?</h5>
        <button className="btn btn-primary" onClick={e => {e.preventDefault(); window.location = '/login' }}>Acceder</button>
        </div>
      </div>
      </div>
    );
  }
});

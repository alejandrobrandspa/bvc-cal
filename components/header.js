'use strict';
import React from 'react';
import jwtDecode from 'jwt-decode';

module.exports = React.createClass({

  closeSession(e) {
    e.preventDefault();
    localStorage.removeItem('x-t');
    window.location= "/";
  },

  render() {
    var token = localStorage.getItem('x-t');
    
    if(token) {
      var user = jwtDecode(token);
    }

    return (
      <div id="header">
        <div className="container">
          <div className="logo pull-left">
            <img src="/img/logo.svg" width="300" alt="BVC logo"/>
          </div>

            <ul className="nav navbar-nav navbar-right">
            <li><a target="_new" href="http://tradersbvc.com.co/subproducto/presentacion-general/26">Información General</a></li>
            <li><a target="_new" href="http://tradersbvc.com.co/subproducto/novedades/28">Novedades del mercado</a></li>
            <li>
              <a href="#" onClick={this.closeSession} className="navbar-right__logout"><i className="icon ion-ios-person-outline"></i> {user.username} <i className="icon ion-log-out"></i></a>
              </li>
           </ul>
        </div>
      </div>
    );
  }
});

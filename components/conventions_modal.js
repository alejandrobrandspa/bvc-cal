'use srict';
import React from 'react';
import Modal from 'react-modal';
import $ from 'jquery';
import Tabs from 'lib/tabs';
import _ from 'lodash';

var customStyle = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(0, 0, 0, 0.75)',
    zIndex: '999'
  },

  content : {
    maxWidth: '600px',
    maxHeight: '400px',
    margin: '0 auto',
    background                 : '#ffffff',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '0',
    outline                    : 'none',
    padding: '50px 0 50px 50px',
    fontWeight: '200'
  }
};

var imgStyle = {
  width: '100%'
};

var contentStyle = {
  padding: '0 15px 30px 15px'
};

var closeStyle = {
  position: 'absolute',
  right: '15px',
  top: '5px',
  fontSize: '32px',
  color: '#E1E1E1'
}

var headerStyle = {
  'background': '#fff',
  'padding': '15px',
  'marginBottom': '15px',
  'borderBottom': '1px solid #E2E2E2'
}

var square = {
  display: 'inline-block',
  marginRight: '11px',
  width: '11px',
  height: '11px'
}

/*let colors = {
  '56d5f5b3d52b07b496e4bed6': '#51B749',1
  '56d5f5d822e6efe796ff01f2': '#3D67AC',2
  '57433bc1afcc67854a8a6d72': '#DC2127',3
  '57433bebafcc67854a8a6d74': '#5484ED',4
  '57433bceafcc67854a8a6d73': '#B90E28',5
  '57435d2ce0861a027247d796': '#FCB62D',6
  '576859f262aa12ad04967c06': '#A91C3E' 7
};*/

var tasaCambio =  {
  display: 'inline-block',
  marginRight: '11px',
  width: '11px',
  height: '11px',
  background: '#51B749'
};

var inflacion =  {
  display: 'inline-block',
  marginRight: '11px',
  width: '11px',
  height: '11px',
  background: '#3D67AC'
};

var acciones =  {
  display: 'inline-block',
  marginRight: '11px',
  width: '11px',
  height: '11px',
  background: '#DC2127'
};

var tes =  {
  display: 'inline-block',
  marginRight: '11px',
  width: '11px',
  height: '11px',
  background: '#5484ED'
};

var colcap =  {
  display: 'inline-block',
  marginRight: '11px',
  width: '11px',
  height: '11px',
  background: '#B90E28'
};

var festivosColombia =  {
  display: 'inline-block',
  marginRight: '11px',
  width: '11px',
  height: '11px',
  background: '#FCB62D'
};

  var festivosUSA =  {
    display: 'inline-block',
    marginRight: '11px',
    width: '11px',
    height: '11px',
    background: '#A91C3E'
  };


module.exports = React.createClass({

  selectValue(e) {
    $(e.currentTarget).select();
  },

  render() {

    return (
      <Modal
        isOpen={this.props.isOpenModal}
        style={customStyle} className="modal-sync">

        <a href="#" onClick={this.props.toggleModal} style={closeStyle}>
          <i className="icon ion-android-close"></i>
        </a>

        <div style={contentStyle} className="content">
        <div className="title" style={{textAlign: "center", marginBottom: '50px'}}>
          <img src="/img/compass.png" alt=""/>
          <h3 style={{fontWeight: '400'}}>Convenciones</h3>
        </div>

          <div className="col-md-6">
            <ul>
            <li><b>UDN:</b> Último día de negociación</li>
            <li><b>LTD:</b> Last trading day.</li>
            <li><b>DE:</b> Día de entrega</li>
            <li><b>DD:</b> Delivery day.</li>
            <li>* Contrato / Contract.</li>
            <li>Dato de inflación / Inflation.</li>
            </ul>
          </div>

          <div className="col-md-6">
            <ul>
              <li><span style={inflacion}></span>INFLACIÓN</li>
              <li><span style={tasaCambio}></span>TASA DE CAMBIO</li>
              <li><span style={acciones}></span>ACCIONES</li>
              <li><span style={colcap}></span>COLCAP</li>
              <li><span style={tes}></span>TES</li>
              <li><span style={festivosColombia}></span>FESTIVOS COLOMBIA</li>
              <li><span style={festivosUSA}></span>FESTIVOS USA</li>
            </ul>
          </div>

        </div>

      </Modal>
    )
  }
});

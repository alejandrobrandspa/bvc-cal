import express from 'express';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import config from './config';
var app = express();

// Express Config
// app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.set('secret', config.secret);
mongoose.connect('mongodb://localhost/calendar');

require('./routes')(app);
app.listen('3000');

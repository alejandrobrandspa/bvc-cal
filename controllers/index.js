'use strict';

module.exports = function index(req, res) {
  var main = __dirname.split('/controllers');
  return res.sendFile(main[0] +"/public/index.html");
};

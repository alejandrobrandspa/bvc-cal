'use strict';
import fs from 'fs';

fs.readdirSync(__dirname)
  .filter(function(file) {
    //remove load.js file
    return (file.indexOf('.') !== 0) && (file !== 'load.js')
  })
  .forEach(function(file) {
    //export each module
    module.exports[file.replace(/\.js$/, '')] = require('./' + file);
  });

'use strict';
import Event from '../models/event';
import api from '../lib/api';
import icsExport from '../lib/ics';
import path from 'path';

var rest = api(Event);

module.exports = {
  all(req, res) {
    rest.all(req, res);
  },

  store(req, res) {
    rest.store(req, res);
  },

  get(req, res) {
    rest.get(req, res)
  },

  update(req, res) {
    rest.update(req, res);
  },

  patch(req, res) {
    rest.updateNested(req, res);
  },

  destroy(req, res) {
    rest.destroy(req, res);
  },

  download(req, res) {
    var calendarOpts = {
      prodid: "-//BVC//BVC Calendar//ES",
      name: "BVC Calendario",
      timezone: "America/Bogota",
      url: "http://104.131.115.146"
    };

    var findByCalendars = req.query.calendars ? { calendar : { $in :  req.query.calendars} }  : {};

    Event
    .find(findByCalendars)
    .exec((err, events) => {
      if(err) {
        return res.status(400).json(err);
      }

      var dir = __dirname.split('controllers')[0];
      var url = path.resolve(dir + 'public/ics/' +'bvc-calendario.ics');


      icsExport(calendarOpts, events, url)
      .then(dest => {
        res.type('text/calendar');
        return res.download(dest);
      })
      .catch(err => {
        return res.status(400).json(err);
      })

    });


  }
}

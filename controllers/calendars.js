'use strict';
import Calendar from '../models/calendar';
import api from '../lib/api';
var rest = api(Calendar);

module.exports = {
  store(req, res) {
    rest.store(req, res);
  },

  all(req, res) {
    rest.all(req, res)
  }
}

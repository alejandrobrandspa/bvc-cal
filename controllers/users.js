'use strict';
import sendgrid from 'sendgrid';
import User from '../models/user';
import api from '../lib/api';
import config from '../config';
import jwt from 'jsonwebtoken';
import exphbs from 'express-handlebars';
import bcrypt from 'bcrypt';

const rest = api(User);
const mail = sendgrid('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');

module.exports = {
  all(req, res) {
    rest.all(req, res)
  },

  store(req, res) {
    rest.store(req, res);
  },

  get(req, res) {
    rest.get(req, res)
  },

  update(req, res) {
    rest.update(req, res);
  },

  destroy(req, res) {
    rest.destroy(req, res);
  },

  login(req, res) {
    var data = req.body;

    User.findOne({email: data.email}, (err, user) => {
      if(err) return res.status(400).json({success: false, message: 'Email o contraseña no valida'});

        if(!user) {
          return res.status(400).json({success: false, message: 'Email o contraseña no valida'})
        };

        if (!user.validPassword(data.password)) {
          return res.status(400).json({success: false, message: 'Email o contraseña no valida' })
        };

        if(user && user._id) {
          var token = jwt.sign({id: user._id,username: user.username, admin: user.admin }, config.secret);
          return res.json({success: true, 'token': token});
        }
    });
  },

  sendConfirmation(req, res) {
    var email = new mail.Email();

    User
    .findOne({_id: req.params.id})
    .exec((err, user) => {
      if(err) return res.status(400).json(err);

      email.addTo(user.email);
      email.setSubject('Confirmar correo');
      email.setHtml('<h3>Bienvenido al Calendario de Derivados BVC</h3> <p>Haga clic en el siguiente vínculo para activar su cuenta.</p>');
      email.setFrom('no-replay@bvc.com.co');
      email.setFromName('Calendario BVC');
      email.addSubstitution('-link-', 'http://calendario.bvc.com.co/confirmation/' + user.id + '?token=' + user.token);
      email.addFilter('templates', 'enable', 1);
      email.addFilter('templates', 'template_id', '8b888f03-a172-434d-91d4-4e7987de4b07');
      mail.send(email, function(err, json){
        if(err) return res.status(400).json(err);
        return res.json(json);
      });
    });
  },

  sendRecover(req, res) {
    var email = new mail.Email();

    User
    .findOne({email: req.query.email})
    .exec((err, user) => {
      if(!user || err) return res.status(400).json(err);

      email.addTo(user.email);
      email.setSubject('Cambiar Contraseña');
      email.setHtml('<h3>Calendario de Derivados BVC</h3> <p>Haga clic en el siguiente vínculo para cambiar su contraseña.</p>');
      email.setFrom('no-replay@bvc.com.co');
      email.setFromName('Calendario BVC');
      email.addSubstitution('-link-', 'http://calendario.bvc.com.co/recover/' + user.id + '?token=' + user.token);
      email.addFilter('templates', 'enable', 1);
      email.addFilter('templates', 'template_id', '8b888f03-a172-434d-91d4-4e7987de4b07');
      mail.send(email, function(err, json){
        if(err) return res.status(400).json(err);
        return res.json(json);
      });
    });
  },

  confirmation(req, res) {
    var id = req.params.id;
    var token = req.query.token;

    User
    .findOne({token: token})
    .exec((err, user) => {
      if(err || !user) return res.status(400).json(err);
      User
      .update({_id: user._id}, {$set: {confirmed: true}})
      .exec((err) => {
        if(err) return res.status(400).json(err);

        return res.json(user);
      });
    });
  },

  recover(req, res) {
    var id = req.params.id;
    var token = req.query.token;
    var password = req.body.password;

    User
    .findOne({token: token}, (err, user) => {
      if(err || !user) return res.status(400).json({message: 'not user'});
      bcrypt.genSalt(10, (err, salt) => {
        if (err) return next(err);

        bcrypt.hash(password, salt, (err, hash) => {
          if (err) return next(err);

          user.update({password: hash}, (updatedErr) => {
            if(err || !user) return res.status(400).json({message: 'error saving'});
            return res.json(user);
          });
        });

    });
  });
}

}

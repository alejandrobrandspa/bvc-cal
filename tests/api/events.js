var supertest = require('supertest');
var request = supertest.agent('localhost:4040');
var expect = require('chai').expect;
var event = {};
var eventToUpdate = {};
var moment = require('moment');

var calendars = [
   '56d5f5a6692b2b969682ac62',
   '56d5f5b3d52b07b496e4bed6',
   '56d5f5caadb75dd3966206cc',
   '56d5f5d822e6efe796ff01f2'
];

var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU2ZWMxMmI5YjJjOGZiM2EzN2U5M2QxYSIsInVzZXJuYW1lIjoiYWxlc2FuYWJyaWF2IiwiYWRtaW4iOnRydWUsImlhdCI6MTQ1OTE5OTcwOH0.RFJ6z3ZvbOxTI-TTJFDLgOd2_N7v1tT8XIOOBN7N55Y';

describe('/api/events', function() {
  it('shoud get all', function(done) {
    request
    .get('/api/events')
    .set('x-access-token', token)
    .expect(200)
    .end(function(err, res) {
      if(err) return done(err);
      event = res.body[0];
      expect(res.body).to.be.a('array');
      done();
    });
  });

  it('should get one', function(done) {
    request
    .get('/api/events/' + event.id)
    .set('x-access-token', token)
    .expect(200)
    .end(function(err, res) {
      if(err) return done(err);
      expect(res.body).to.be.a('object');
      expect(res.body.title).to.equal(event.title);
      done();
    });
  });

  it('should store a event', function(done) {
    var newEvent = {
      calendar: calendars[0],
      title: 'from tests api',
      description: 'Lorem ipsum my friend',
      location: 'chai mocha',
      start: moment().format('YYYY-MM-DD'),
      end: moment().format('YYYY-MM-DD')
    };

    request
    .post('/api/events/')
    .set('x-access-token', token)
    .send(newEvent)
    .expect(201)
    .end(function(err, res) {
      if(err) return done(err);
      eventToUpdate = res.body;
      expect(res.body).to.be.a('object');
      expect(res.body.title).to.equal(newEvent.title);
      expect(res.body.description).to.equal(newEvent.description);
      expect(res.body.location).to.equal(newEvent.location);
      expect(res.body.start).to.equal(newEvent.start);
      expect(res.body.end).to.equal(newEvent.end);
      done();
    });
  });

  it('should update a event', function(done) {
    done(eventToUpdate);
  });

});

var _ = require('lodash');
var url = 'localhost:4040';
 var calendars = [
    '56d5f5a6692b2b969682ac62',
    '56d5f5b3d52b07b496e4bed6',
    '56d5f5caadb75dd3966206cc',
    '56d5f5d822e6efe796ff01f2'
];

module.exports = {
  beforeEach(client) {
    client
      .url('localhost:4040')
      .waitForElementVisible('body', 3000)
      .setValue('input[type=text]', 'alesanabriav@gmail.com')
      .setValue('input[type=password]', 'durden99')
      .click('.login-btn')
      .pause(1000);
  },

  createEvent(client) {

    client
      .url(url + '/admin')
      .pause(1000)
      .click('#events-admin .panel-heading a')
      .pause(3000)
      .click('#event-form .Select')
      .click('.Select-menu-outer')
      .setValue('[name=title]', 'title to test')
      .setValue('[name=description]', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
      .setValue('[name=location]', 'location BVC')
      .click('.publish')
      .pause(1000)
      .assert.containsText('.list li:nth-child(2)', 'title to test')
      .end();
  }
}

'use strict';
var mongoose = require('mongoose');

var schema = {
  title: String
};

module.exports = mongoose.model('Calendar', schema);

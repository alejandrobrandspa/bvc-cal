'use strict';
import mongoose from 'mongoose';
import moment from 'moment';

var schema = new mongoose.Schema({
  calendar: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Calendar',
    required: 'Calendario requerido'
  },
  'title': {
    type: String,
    required: 'Título requerido',
  },
  'description': String,
  'location': String,
  'start': { type: String},
  'end': { type: String},
  alerts: [
    {
      num: Number,
      str: String,
      createdAt: Date
    }
  ],
  updatedAt: {
    type: Date,
    default: Date.now()
  }
});


schema.set('toJSON', {
  transform: function (doc, ret) {
   ret.id = ret._id;
   delete ret._id;
   delete ret.__v;
  }
});

module.exports = mongoose.model('Event', schema);

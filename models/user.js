'use strict';
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

var schema = new mongoose.Schema({
  identification: {
    type: String,
    required: 'Cédula requerida',
  },
  name: {
    type: String,
    required: 'Nombre requerido'
  },
  lastname: {
    type: String,
    required: 'Apellido requerido'
  },
  email: {
    type: String,
    required: 'Email requerido'
  },
  phone: String,
  mobile: String,
  company: String,
  position: String,
  username: String,
  password: {
    type: String,
    required: 'Contraseña requerida'
  },
  token: String,
  confirmed: {
    type: Boolean,
    default: false
  },
  admin: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: Date
});

schema.set('toJSON', {
  transform: function (doc, ret) {
   ret.id = ret._id;
   delete ret.password;
   delete ret.token;
   delete ret.__v;
  }
});

schema.methods.validPassword = function(password) {
  var user = this;
  return bcrypt.compareSync(password, user.password);
};

schema.pre('save', function(next) {
  var user = this;
  var username = user.email.split('@');
  user.username = username[0];
  user.updated = Date.now();

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) return next(err);

      user.token = salt + Date.now();
      user.password = hash;
      next();
    });
  });
});

schema.pre('update', function(next) {
  var user = this;

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);
    this.update({}, {$set: {
       updatedAt: Date.now(),
       token: salt + Date.now()
     }});
    next();
  });
});

// schema.pre('update', function(next) {
//   var user = this;
//
//   bcrypt.genSalt(10, (err, salt) => {
//     if (err) return next(err);
//       user.updatedAt = Date.now();
//       user.token =  salt + Date.now();
//       user.password = hash;
//       next();
//   });
//
//
// });

module.exports = mongoose.model('User', schema);

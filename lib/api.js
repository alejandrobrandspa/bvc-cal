'use strict';

module.exports = function api(model) {
  return {
    all(req, res) {
      var popu = req.query.populate ? req.query.populate : '';
      var fin = req.query.find ? JSON.parse(req.query.find) : {};

      model
      .find(fin)
      .populate(popu)
      .sort({'start': -1})
      .exec((err, collection) => {
        if(err) {
          return res.status(401).json(err);
        }
        return res.status(200).json(collection);
      });
    },

    store(req, res) {
      var data = req.body;
      var Model = new model(data);
      var popu = req.query.populate ? req.query.populate : '';

      Model.save((err, result) => {
        if(err) return res.status(400).json(err);

          model
          .findOne({_id: result._id})
          .populate(popu)
          .exec((err, item) => {
            if(err) return res.status(400).json(err);

            return res.status(201).json(item);
          });
      });
    },

    get(req, res) {
      var popu = req.query.populate ? req.query.populate : '';

      model
      .findOne({_id: req.params.id})
      .populate(popu)
      .exec((err, result) => {
        if(err) return res.status(400).json(err);
        return res.status(200).json(result);
      });
    },

    update(req, res) {
      var popu = req.query.populate ? req.query.populate : '';
      var id = req.params.id;
      model
      .update({_id: id}, req.body , {runValidators: true})
      .exec((err, result) => {
        if(err) return res.status(400).json(err);

          model
          .findOne({_id: id})
          .populate(popu)
          .exec((err, item) => {
            if(err) return res.status(400).json(err);
            return res.status(200).json(item);
          });

      });
    },

    updateNested(req, res) {
      var popu = req.query.populate ? req.query.populate : '';
      var id = req.params.id;

      model
      .update({_id: id}, {$addToSet: req.body})
      .exec(function(err, result) {
        if(err) return res.status(400).json(err);
          model
          .findOne({_id: id})
          .populate(popu)
          .exec(function(err, item) {
            if(err) return res.status(400).json(err);
            return res.status(200).json(item);
          });
      });
    },

    destroy(req, res) {
      model.findByIdAndRemove(req.params.id, (err) => {
        if(err) return res.status(400).json(err);
        return res.status(200).json({deleted: req.params.id});
      });
    }
  }
}

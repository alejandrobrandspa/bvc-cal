'use strict';
import passport from 'passport';
import {Strategy} from 'passport-local';
import User from '../models/user';

var local = new Strategy({usernameField: 'email'}, (username, password, done) => {
  User.findOne({email: username}, (err, user) => {
    if(err) return done(err);

    if(!user) {
      return done(null, false, {message: 'Error en usuario o contraseña'})
    };

    if (!user.validPassword(password)) {
      return done(null, false, { message: 'Error en usuario o contraseña' })
    };

    return done(null, user);
  });
});

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser((id, done) => {
  User.findOne({_id: id}, (err, user) => {
      done(null, user);
  });
});

passport.use(local);


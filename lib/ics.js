'use strict';
var moment = require('moment-timezone');
var fs = require('fs');
var path = require('path');
var TMPDIR = require('os').tmpdir();
var replacePlaceholder = require('./lib/replacePlaceholder');
var parseTrigger = require('./lib/parseTrigger');
var _ = require('lodash');

function Calendar(options, events) {
  this.options = options;
  var evts = this.setEvents(events, options);
  this.setCalendar(options, evts);
}

Calendar.prototype.setCalendar = function(options, events) {
  this.calendar = [];
  var c = this.calendar;
  c.push("BEGIN:VCALENDAR");
  c.push("VERSION:2.0");
  c.push("PRODID:-//BVC//NONSGML BVC CALENDAR//ES")
  c.push('X-PUBLISHED-TTL:PT1H');
  c.push('X-ORIGINAL-URL:' + options.url);
  c.push("CALSCALE:GREGORIAN");
  c.push("METHOD:PUBLISH");
  c.push("X-WR-CALNAME:" + options.name);
  c.push("X-WR-TIMEZONE:" + options.timezone);
  c.push("PLACEHODER-EVENTS");
  c.push("END:VCALENDAR");
  c = replacePlaceholder(c, "PLACEHODER-EVENTS", events);
};

Calendar.prototype.getCalendar = function() {
  return this.calendar.join('\r\n');
};

Calendar.prototype.setAlarm = function(a) {
  var alarm = [];
  var trigger = parseTrigger(a) || "1H";
  alarm.push("BEGIN:VALARM");
  alarm.push("ACTION:DISPLAY");
  alarm.push("DESCRIPTION:" + a.description);
  alarm.push("TRIGGER:" + trigger);
  alarm.push("END:VALARM");
  console.log(alarm);
  return alarm.join('\r\n');
};

Calendar.prototype.setAlarms = function(event) {
  var _this = this;
  var alerts = event.alerts;
  var item;

  var result = alerts.map(function(alarm) {
    item = _.extend(alarm, {description: event.title});
    return _this.setAlarm(item);
  });

  return result.join('\r\n');
};

Calendar.prototype.setEvent = function(e) {
  var event = [];
  var tz = this.options.timezone || 'America/New_York';
  var uid = e._id || Math.random().toString(35).substr(2, 10);
  var title = e.title || "Event Title";
  var description = e.description || "";
  var location = e.location || "";
  var format = e.format;
  var start = moment(e.start, format) || moment();
  var end = moment(e.end, format) || moment();
  var startFormatted = "DTSTART:" + start.format('YYYYMMDDTHHmm').concat('00');
  var endFormatted = "DTEND:" + end.format('YYYYMMDDTHHmm').concat('00');

  if(start._f == 'YYYY-MM-DD') {
    startFormatted = "DTSTART;VALUE=DATE:" + start.format('YYYYMMDD');
    endFormatted = "DTEND;VALUE=DATE:" + end.format('YYYYMMDD');
  }

  event.push("BEGIN:VEVENT");
  event.push("UID:" + uid + '@bvc.com.co');
  event.push("DTSTAMP:" + moment().format('YYYYMMDDTHHmm').concat('00'));
  event.push(startFormatted);
  event.push(endFormatted);
  event.push("SUMMARY:" + title);

  if(description) {
    event.push("DESCRIPTION:" + description);
  }

  if(location) {
    event.push("LOCATION:" + location);
  }

  if(this.setAlarms(e)) {
    event.push("PLACEHODER-ALERTS");
    event = replacePlaceholder(event, "PLACEHODER-ALERTS", this.setAlarms(e));
  }

  event.push("END:VEVENT");

  return event.join('\r\n');
};

Calendar.prototype.setEvents = function(events) {
  var result = events.map(function(event) {
    return this.setEvent(event);
  }.bind(this));

  return result.join('\r\n');
};

function getFile(options, events, pathParam, cb) {
  var calendar = new Calendar(options, events);
  var data = calendar.getCalendar();
  var dest = pathParam || path.join(TMPDIR, 'events.ics');

   var file = new Promise((resolve, reject) => {
     fs.writeFile(dest, data, function(err) {
      if (err) return reject(err);
      return resolve(dest);
     });
   });

   return file;
}

module.exports = getFile;

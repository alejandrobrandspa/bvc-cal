'use strict';
import express from 'express';
import controller from './controllers/load.js';
import validate from './lib/validate';

module.exports = function(app) {
  //without token
  app.get('/', controller.index);
  app.get('/register', controller.index);
  app.get('/test-component', controller.index);
  app.get('/login', controller.index);
  app.get('/admin', controller.index);
  app.get('/admin/*', controller.index);
  app.get('/admin/login', controller.index);
  app.get('/confirmation/:id', controller.index);
  app.get('/recover/:id', controller.index);

  //register
  app.post('/api/users', controller.users.store);
  app.post('/api/users/recover/:id', controller.users.recover);
  app.post('/api/users/sendrecover', controller.users.sendRecover);
  app.get('/api/users/sendconfirmation/:id', controller.users.sendConfirmation);
  app.get('/api/users/confirmation/:id', controller.users.confirmation);

  //login
  app.post('/api/auth', controller.users.login);

  app.use(validate);

  // Users
  app.get('/api/users', controller.users.all);

  // Events
  app.get('/api/events', controller.events.all);
  app.get('/api/events/download', controller.events.download);
  app.get('/api/events/:id', controller.events.get);
  app.post('/api/events', controller.events.store);
  app.put('/api/events/:id', controller.events.update);
  app.patch('/api/events/:id', controller.events.patch);
  app.delete('/api/events/:id', controller.events.destroy);

  // Calendars
  app.get('/api/calendars', controller.calendars.all);
  app.post('/api/calendars', controller.calendars.store);
}

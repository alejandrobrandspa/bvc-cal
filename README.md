# BVC CALENDARIO

## Servidor
- Ubuntu 14.04 o mayor
- Nodejs 6 o mayor
- Mongodb 3
- pm2 

## Backend
- Expressjs
- Mongoose

## Frontend
- React 
- Jquery
- Bootstrap

## tools
- babel
- nightwatch